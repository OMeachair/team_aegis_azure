using System;
using Microsoft.AspNetCore.Mvc;
using BarbarAPI.Models;
using System.Diagnostics;
using System.Configuration;
//using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;

namespace BarbarAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TraineeController : ControllerBase
    {
        //GET api/trainee/traineeLogIn
        [HttpPost("logIn")]
        public JObject PostLogIn([FromBody] Trainee trainee)
        {
            JObject responseObj = new JObject();
            UserController userController = new UserController();

            SqlConnection conn = null;
            SqlDataReader reader = null;
            SqlCommand checkCredentials;

            String connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            string hPassword = userController.ComputeHash(trainee.password, new SHA256CryptoServiceProvider());

            String query = "Select * from trainee where trainee_username = '" + trainee.username + "' and trainee_password = '" + hPassword + "';";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL ");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                checkCredentials = new SqlCommand(query, conn);
                int count = Convert.ToInt32(checkCredentials.ExecuteScalar());

                Debug.Write("INTEGER RETURNED: " + count);

                if (count > 0)
                {
                    reader = checkCredentials.ExecuteReader();

                    while (reader.Read())
                    {

                        trainee.id = reader.GetInt32(0);
                        trainee.username = reader.GetString(1);
                        trainee.firstname = reader.GetString(2);
                        trainee.surname = reader.GetString(3);
                        trainee.email = reader.GetString(4);
                        trainee.password = reader.GetString(5);
                        trainee.avatar = reader.GetString(6);
                    }
                    Debug.Write("LOGIN SUCCESSFUL ");
                    Debug.Write("CONNECTION CLOSED ");

                    responseObj.Add("message", "Success");
                    responseObj.Add("data", trainee.toJson());
                }
                else
                {
                    Debug.Write("FAILED TO LOG IN INCORRECT CRDENTIALS");


                    responseObj.Add("message", "failed");
                    responseObj.Add("data", trainee.toJson());
                    responseObj.Add("error", "Failed to log in");
                }
                conn.Close();
                Debug.Write("CONNECTION CLOSED ");


            }
            catch (Exception e)
            {
                Debug.Write("LOGIN FAILED: " + e);

                responseObj.Add("message", "failed");
                responseObj.Add("data", trainee.toJson());
                responseObj.Add("error", e.GetBaseException().ToString());

            }
            return responseObj;
        }

        // POST api/trainee/traineeSignUp
        [HttpPost("signUp")]
        public JObject PostSignUp([FromBody] Trainee trainee)
        {
            JObject responseObj = new JObject();
            UserController userController = new UserController();

            SqlConnection conn = null;
            SqlCommand comm;
            SqlCommand checkEmail;
            SqlCommand checkUsername;

            String connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            string hPassword = userController.ComputeHash(trainee.password, new SHA256CryptoServiceProvider());

            String query = "INSERT INTO trainee(trainee_username,trainee_name,trainee_surname,trainee_email," +
                "trainee_password,trainee_avatar) VALUES ('" + trainee.username + "','" +
                trainee.firstname + "','" + trainee.surname + "','" + trainee.email + "','" + hPassword + "','" +
                trainee.avatar + "'); ";

            String query2 = "Select * from trainee where trainee_email = '" + trainee.email + "';";

            String query3 = "Select * from trainee where trainee_username = '" + trainee.username + "';";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                comm = new SqlCommand(query, conn);

                checkEmail = new SqlCommand(query2, conn);
                int count = Convert.ToInt32(checkEmail.ExecuteScalar());

                checkUsername = new SqlCommand(query3, conn);
                int count1 = Convert.ToInt32(checkUsername.ExecuteScalar());
                if (count > 0)
                {
                    Debug.Write("USER ALREADY EXSISTS ");
                    responseObj.Add("message", "failed");
                    responseObj.Add("data", trainee.toJson());
                    responseObj.Add("error", "Email already taken");
                }
                else if (count1 > 0)
                {
                    Debug.Write("USER ALREADY EXSISTS ");
                    responseObj.Add("message", "failed");
                    responseObj.Add("data", trainee.toJson());
                    responseObj.Add("error", "Username already taken");
                }
                else
                {
                    comm.ExecuteNonQuery();
                    Debug.Write("SIGNUP: INSERT SUCCESSFUL");
                    responseObj.Add("message", "success");
                    responseObj.Add("data", trainee.toJson());
                }
                conn.Close();
                Debug.Write("CONNECTION CLOSED ");
            }
            catch (Exception e)
            {
                Debug.Write("SIGNUP: FAILED TO INSERT: " + e);

                responseObj.Add("message", "failed");
                responseObj.Add("data", trainee.toJson());
                responseObj.Add("error", e.GetBaseException().ToString());
            }

            return responseObj;
        }

        //POST api/trainee/HaircutInformation
        //Gets haircut name,description,image and object id
        [HttpPost("HaircutInformation")]
        public JObject GetHaircutInformation([FromBody] Haircut haircut)
        {
            JObject responseObj = new JObject();
            SqlConnection conn = null;
            SqlCommand comm;
            SqlDataReader reader = null;

            string connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            string query = "SELECT haircut_name, haircut_image_url, haircut_description, object_data_id FROM object_data WHERE object_data_id = '" + haircut.Object_data_id + "';";



            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {

                comm = new SqlCommand(query, conn);

                comm.ExecuteNonQuery();
                reader = comm.ExecuteReader();
                JObject session_data = null;
                JArray session_data_list = new JArray();

                string object_name = haircut.Object_name;
                string haircut_url = haircut.Haircut_url;
                string haircut_description = haircut.Haircut_description;
                int object_data_id = haircut.Object_data_id;
                while (reader.Read())
                {
                    object_name = reader.GetString(0);
                    haircut_url = reader.GetString(1);
                    haircut_description = reader.GetString(2);
                    object_data_id = reader.GetInt32(3);


                    session_data = new JObject();
                    session_data.Add("haircut_name", object_name);
                    session_data.Add("haircut_url", haircut_url);
                    session_data.Add("haircut_description", haircut_description);
                    session_data.Add("object_data_id", object_data_id);

                    session_data_list.Add(session_data);

                }
                responseObj.Add("message", "success");
                responseObj.Add("data", session_data_list);

                Debug.Write("INSERT SUCCESSFUL ");
                conn.Close();
                Debug.Write("CONNECTION CLOSED ");

            }

            catch (Exception e)
            {
                Debug.Write("FAILED TO GET HAIRCUT DETAILS " + e);
                responseObj.Add("message", "failed");
                responseObj.Add("data", haircut.ToString());
                responseObj.Add("error", e.GetBaseException().ToString());
            }
            return responseObj;
        }

        // POST api/trainee/updateTraineeInfo  
        [HttpPost("updateTraineeInfo")]
        public JObject UpdateTrainee([FromBody] Trainee trainee)
        {
            JObject responseObj = new JObject();
            SqlConnection conn = null;
            SqlCommand comm;

            String connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            String query = "UPDATE trainee set trainee_email='" + trainee.email + "', trainee_username='" + trainee.username +
                            "', trainee_avatar='" + trainee.avatar + "', trainee_password='" + trainee.password + "' where trainee_id='" + trainee.id + "';";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {


                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                comm = new SqlCommand(query, conn);
                comm.ExecuteNonQuery();


                Debug.Write("TRAINEE UPDATED");
                conn.Close();
                Debug.Write("CONNECTION CLOSED");

                responseObj.Add("message", "Success");
                responseObj.Add("data", trainee.ToString());
                responseObj.Add("TRAINEE UPDATED");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO UPDATE TRAINEE " + e);
                responseObj.Add("message", "failed");
                responseObj.Add("data", trainee.ToString());
                responseObj.Add("error", e.GetBaseException().ToString());
            }

            return responseObj;
        }
		
		
        // POST api/trainee/SelfAssesment
        [HttpPost("SelfAssesment")]
        public JObject PostSelfAssesment([FromBody] JObject assessment)
        {
            JObject responseObj = new JObject();

            SqlConnection conn = null;
            SqlCommand comm;

            string connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;
            int session_id = 0;
            int trainee_id = 0;
            string self_assessment = "";
            int rating = 0;

            if (assessment.GetValue("session_id") != null && !assessment.GetValue("session_id").Equals(""))
            {
                session_id = Int32.Parse(assessment.GetValue("session_id").ToString());
            }
            if (assessment.GetValue("trainee_id") != null && !assessment.GetValue("trainee_id").Equals(""))
            {
                trainee_id = Int32.Parse(assessment.GetValue("trainee_id").ToString());
            }
            if (assessment.GetValue("self_assessment") != null && !assessment.GetValue("self_assessment").Equals(""))
            {
                self_assessment = assessment.GetValue("self_assessment").ToString();
            }
            if (assessment.GetValue("rating") != null && !assessment.GetValue("rating").Equals(""))
            {
                rating = Int32.Parse(assessment.GetValue("rating").ToString());
            }
            string query = "INSERT INTO trainee_assessment (session_id, trainee_id, self_assessment, rating)" +
                " VALUES ('" + session_id + "', '" + trainee_id + "', '" + self_assessment + "', '" + rating + "');";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                comm = new SqlCommand(query, conn);
                comm.ExecuteNonQuery();

                Debug.Write("SELF ASSESSMENT UPDATED");
                conn.Close();
                Debug.Write("CONNECTION CLOSED");

                responseObj.Add("message", "success");
                responseObj.Add("data", assessment);

            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO UPDATE SELF ASSESSMENT " + e);
                responseObj.Add("message", "failed");
                responseObj.Add("data", assessment.ToString());
                responseObj.Add("error", e.GetBaseException().ToString());
            }
            return responseObj;
        }

		//POST api/trainee/HaircutInfo
        [HttpPost("HaircutInfo")]
        public JObject GetHaircutInfo([FromBody] Trainee trainee)
        {
            JObject responseObj = new JObject();
            SqlConnection conn = null;
            SqlCommand comm;
            SqlDataReader reader = null;

            string connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            string query = "SELECT time_taken, haircut_id FROM haircut_session WHERE trainee_id = '" + trainee.id + "';";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                comm = new SqlCommand(query, conn);
                comm.ExecuteNonQuery();

                reader = comm.ExecuteReader();

                string time_taken = "";
                int haircut_id = 0;
                JObject session_data = null;
                JArray session_data_list = new JArray();
                while (reader.Read())
                {
                    JObject haircut_details = new JObject();


                    time_taken = reader.GetString(0);
                    haircut_id = reader.GetInt32(1);

                    session_data = new JObject();
                    session_data.Add("time_taken", time_taken);
                    haircut_details = GetHaircutDetails(haircut_id);
                    session_data.Add("haircut_details", haircut_details);

                    session_data_list.Add(session_data);
                }

                responseObj.Add("message", "success");
                responseObj.Add("data", session_data_list);

                Debug.Write("SUCCESSFUL");
                conn.Close();
                Debug.Write("CONNECTION CLOSED");

            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO GET HAIRCUT HISTORY " + e);
                responseObj.Add("message", "failed");
                responseObj.Add("data", trainee.ToString());
                responseObj.Add("error", e.GetBaseException().ToString());
            }
            return responseObj;
        }

		 public JObject GetHaircutDetails(int haircut_id)
        {
            JObject haircut_data = new JObject();
            SqlConnection conn = null;
            SqlCommand comm;
            SqlDataReader reader = null;

            string connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            string query = "Select haircut_image_url, haircut_name from object_data where object_data_id = '" + haircut_id + "';";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");
            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO CONNECT: " + e);
            }

            try
            {
                comm = new SqlCommand(query, conn);
                comm.ExecuteNonQuery();

                reader = comm.ExecuteReader();

                string haricut_url = "";
                string haircut_name = "";

                while (reader.Read())
                {

                    haricut_url = reader.GetString(0);
                    haircut_name = reader.GetString(1);

                    haircut_data.Add("haircut_url", haricut_url);
                    haircut_data.Add("haircut_name", haircut_name);
                }


                conn.Close();

            }
            catch (Exception e)
            {
                Debug.Write("FAILED TO GET HAIRCUT HISTORY " + e);
            }
            return haircut_data;
        }
    }

}


