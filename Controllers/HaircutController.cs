﻿using Microsoft.AspNetCore.Mvc;
//using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BarbarAPI.Controllers
{
    [Route("api/[controller]")]
    public class HaircutController : Controller
    {
        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }



        // POST api/haircut/session/complete
        [HttpPost("session/complete")]
        public JObject InsertSessionData([FromBody] JObject obj)
        {
            JObject responseObj = new JObject();

            string connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            SqlConnection conn = null;
            SqlCommand comm1,comm2;

            string code = "";
            string trainee_id = "";
            string time_taken = "";

            if (obj.GetValue("code") != null && !obj.GetValue("code").Equals(""))
            {
                code = obj.GetValue("code").ToString();
            }
            if (obj.GetValue("trainee_id") != null && !obj.GetValue("trainee_id").Equals(""))
            {
                trainee_id = obj.GetValue("trainee_id").ToString();
            }
            if (obj.GetValue("time_taken") != null && !obj.GetValue("time_taken").Equals(""))
            {
                time_taken = obj.GetValue("time_taken").ToString();
            }

            string query1 = "INSERT INTO haircut_session(trainee_id,time_taken) VALUES ('" + trainee_id + "','" +
                time_taken + "'); ";

            string query2 = "UPDATE haircut_session_code SET isFinished  = 1 WHERE code= '"+code +"';";

            if (!trainee_id.Equals("") && !time_taken.Equals(""))
            {
                try
                {
                    conn = new SqlConnection(connectionString);
                    conn.Open();
                    Debug.Write("CONNECTION SUCCESSFUL");

                    comm1 = new SqlCommand(query1, conn);
                    comm1.ExecuteNonQuery();
                    Debug.Write("INSERT SUCCESSFUL");

                    comm2 = new SqlCommand(query2, conn);
                    comm2.ExecuteNonQuery();
                    Debug.Write("UPDATE SUCCESSFUL");

                    responseObj.Add("message", "success");
                    responseObj.Add("data", obj);

                    return responseObj;

                }
                catch (Exception e)
                {
                    Debug.Write("FAILED TO INSERT: " + e);

                    responseObj.Add("message", "failed");
                    responseObj.Add("data", obj);
                    responseObj.Add("error", e.GetBaseException().ToString());

                    return responseObj;
                }
            }
            else
            {
                responseObj.Add("message", "failed");
                responseObj.Add("data", obj);
                responseObj.Add("error", "failed to retrieve trainee id or time taken");

                return responseObj;
            }

        }

        //GET api/barber/code
        [HttpPost("validateCode")]
        public JObject ValidateCode([FromBody] JObject codeInput)
        {
            SqlConnection conn = null;
            SqlCommand comm;
            SqlDataReader reader = null;

            JObject responseObj = new JObject();
            JObject codeObj = new JObject();

            string code = codeInput.GetValue("code").ToString();

            String connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            String query = "SELECT * FROM haircut_session_code WHERE code = '" + code + "'; ";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");

                comm = new SqlCommand(query, conn);
                int db_data = Convert.ToInt32(comm.ExecuteScalar());
                reader = comm.ExecuteReader();

                Debug.Write("DB: " + db_data);

                if (db_data > 0)
                {
                    while (reader.Read())
                    {
                        codeObj.Add("id", reader.GetInt32(0));
                        codeObj.Add("code", reader.GetString(1));
                        codeObj.Add("username", reader.GetString(2));
                        codeObj.Add("haircut", reader.GetString(3));
                    }

                    responseObj.Add("message", "Success");
                    responseObj.Add("data", codeObj);
                }
                else
                {
                    responseObj.Add("message", "failed");
                    responseObj.Add("data", code);
                    responseObj.Add("error", "code incorrect");
                }
                conn.Close();
                Debug.Write("CONNECTION CLOSED");

            }
            catch (Exception e)
            {
                Debug.Write(e);

                responseObj.Add("message", "failed");
                responseObj.Add("data", code);
                responseObj.Add("error", e.GetBaseException().ToString());
            }

            return responseObj;
        }

        //GET api/barber/code
        [HttpPost("session/checkStatus")]
        public JObject checkSessionStatus([FromBody] JObject codeObj)
        {
            SqlConnection conn = null;
            SqlCommand comm;
            SqlDataReader reader = null;

            JObject responseObj = new JObject();

            int isFinished = 2;

            string code = "";

            if (codeObj.GetValue("code") != null && !codeObj.GetValue("code").Equals(""))
            {
                code = codeObj.GetValue("code").ToString();
            }

            String connectionString = ConfigurationManager.ConnectionStrings["azure"].ConnectionString;

            String query = "SELECT code_id, isFinished FROM haircut_session_code WHERE code = '" + code + "'; ";

            try
            {
                conn = new SqlConnection(connectionString);
                conn.Open();
                Debug.Write("CONNECTION SUCCESSFUL");

                comm = new SqlCommand(query, conn);
                int db_data = Convert.ToInt32(comm.ExecuteScalar());
                reader = comm.ExecuteReader();

                if (db_data > 0)
                {
                    while (reader.Read())
                    {
                        isFinished = reader.GetInt32(1);
                    }

                    if (isFinished == 0 || isFinished == 1)
                    {
                        responseObj.Add("message", "Success");
                        responseObj.Add("data", isFinished);
                    }
                    else
                    {
                        responseObj.Add("message", "failed");
                        responseObj.Add("data", code);
                        responseObj.Add("error", "session not found");
                    }

                }
                else
                {
                    responseObj.Add("message", "failed");
                    responseObj.Add("data", code);
                    responseObj.Add("error", "session not found");
                }
                conn.Close();
                Debug.Write("CONNECTION CLOSED");

            }
            catch (Exception e)
            {
                Debug.Write(e);

                responseObj.Add("message", "failed");
                responseObj.Add("data", code);
                responseObj.Add("error", e.GetBaseException().ToString());
            }

            return responseObj;
        }
    }
}
