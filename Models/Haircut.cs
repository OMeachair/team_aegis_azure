﻿using Newtonsoft.Json.Linq;
using System;

namespace BarbarAPI.Models
{
    public class Haircut
    {
        public int Object_data_id { get; set; }
        public string Object_name { get; set; }
        public string Vertices { get; set; }
        public string Normals { get; set; }
        public string Texture_coordinates { get; set; }
        public string Indices { get; set; }
        public string Haircut_name { get; set; }
        public string Haircut_url { get; set; }
        public string Haircut_description { get; set; }

        public int Rating { get; set; }
        public string Self_assessment { get; set; }
        public int Session_id { get; set; }

        public string time_taken { get; set; }

        public JObject toJson()
        {
            JObject haircut = new JObject();

            haircut.Add("Object_data_id", Object_data_id);
            haircut.Add("Object_name", Object_name);
            haircut.Add("Vertices", Vertices);
            haircut.Add("Normals", Normals);
            haircut.Add("Texture_coordinates", Texture_coordinates);
            haircut.Add("Indices", Indices);
            haircut.Add("Haircut_name", Haircut_name);
            haircut.Add("Haircut_url", Haircut_url);
            haircut.Add("Haircut_description", Haircut_description);
            haircut.Add("Rating", Rating);
            haircut.Add("Self_assessment", Self_assessment);
            haircut.Add("Session_id", Session_id);
            haircut.Add("time_taken", time_taken);

            return haircut;
        }
    }
}
